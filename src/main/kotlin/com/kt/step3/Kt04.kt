package com.kt.step3

//TODO List集合遍历
fun main() {
    val list = listOf(1, 2, 3, 4, 5, 6)
    for (i in list) {
        print("$i ")
    }
    println()
    list.forEach(){
        print("$it ")
    }
    println()
    list.forEachIndexed(){ i: Int, i1: Int ->
        print(" index=$i, item=$i1")
    }

}