package com.kt.step3

import java.security.cert.PKIXReason

//TODO Set创建与元素的获取
fun main() {
    //Set 不会有重复元素，有重复的会自动去掉
    val set1 = setOf("A", "B", "C", "C")
    println(set1.elementAt(0))
    println(set1.elementAtOrElse(3) { "越界了" })
    println(set1.elementAtOrNull(3)?:"越界干啥")
}