package com.kt.step3

import java.io.File

//TODO 数组类型
fun main() {
    var intArrayOf = intArrayOf(1, 2, 3, 4)
    println(intArrayOf[0])
    println(intArrayOf.elementAtOrElse(4) { -1 })
    println(intArrayOf.elementAtOrNull(4)?: -2)

    //集合转数组
    var listOf = listOf(1, 2, 3)
    var toIntArray = listOf.toIntArray()
    println(toIntArray)

    //对象数组
    var arrayOf : Array<File> = arrayOf(File("A.txt"), File("B.txt"))
}