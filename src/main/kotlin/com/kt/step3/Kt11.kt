package com.kt.step3
//TODO 读取map的值
fun main() {
    val map1 = mapOf("a" to 2, "b" to 6)
    //方式一 []
    println(map1["a"])
    println(map1["c"]) //找不到返回null
    //方式二 getOrDefault 找不到给一个默认值
    println(map1.getOrDefault("a", -1))
    //方式三 getOrElse 找不到执行一个匿名函数（lambda表达式）
    println(map1.getOrElse("b") { -1 })
    //方式四 找不到会crash，避免使用该方式
    println(map1.getValue("a"))
    println(map1.getValue("c"))
}