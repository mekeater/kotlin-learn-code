package com.kt.step3
//TODO List创建与元素获取
fun main() {
    //完整定义
    //val listValue:List<String> = listOf<String>("A", "B", "C","D")
    //类型推断后
    val listValue = listOf("A", "B", "C","D")
    //索引获取元素值，存在索引越界的异常情况
    println(listValue[0])
    //通过getOrElse或者getOrNull提前处理索引异常情况
    println(listValue.getOrElse(4) {"默认值"})
    println(listValue.getOrNull(4) ?: "超出索引界限")

}