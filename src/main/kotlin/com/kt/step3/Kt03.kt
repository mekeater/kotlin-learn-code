package com.kt.step3

//TODO mutator函数，具有+= -=的运算符重载
fun main() {
    val list1 = mutableListOf("A", "B", "C")
    list1 += "D"
    list1 += "E"
    println(list1)
    //removeIf 带条件移除元素
    list1.removeIf { it.contains("A") }
    println(list1)
}