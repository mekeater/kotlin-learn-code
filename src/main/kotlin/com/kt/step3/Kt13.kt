package com.kt.step3
//TODO 可变Map集合
fun main() {
    val map1 = mutableMapOf("a" to(1), "b" to 2)
    //添加一个键值对
    map1 += "c" to 3
    //移除一个键值对
    map1 -= "a"
    //如果没有就put进去一个key value
    map1.getOrPut("d") { 4 }
    map1.forEach { (key, value) ->
        println("key=$key,value=$value")
    }
}