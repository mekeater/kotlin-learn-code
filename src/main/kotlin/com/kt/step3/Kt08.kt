package com.kt.step3
//TODO 集合转换与快捷函数distinct
fun main() {
    var mutableListOf = mutableListOf("A", "B", "C", "C")
    println(mutableListOf)
    //List转Set集合，去重
    var toSet = mutableListOf.toSet()
    println(toSet)
    println(mutableListOf.toSet().toList())
    //distinct去重
    println(mutableListOf.distinct())
}