package com.kt.step3
//TODO 可变List集合
fun main() {
    val list1 = mutableListOf("A","B", "C")
    list1.add("D")
    list1.remove("B")
    println(list1)

    //可变集合与不可变集合可以相互转换
    println(list1.toList())
}
