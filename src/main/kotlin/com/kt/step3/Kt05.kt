package com.kt.step3
//TODO 解构语法 过滤元素
fun main() {
    val list = listOf(1, 2, 3)
    // 解构造语法
    val(v1,v2,v3) = list
    println("$v1,$v2,$v3")
    //过滤元素
    var(_,v4,v5) = list
    println("$v4,$v5")
}