package com.kt.step3
//TODO 遍历Map
fun main() {
    val map1 = mapOf("a" to 2, "b" to 6)
    map1.forEach {
        println("key=${it.key},value=${it.value}")
    }
    println()

    map1.forEach { key, value ->
        println("key=$key,value=$value")
    }
    println()

    map1.forEach { (key, value) ->
        println("key=$key,value=$value")
    }
    println()

    for (entry in map1) {
        println("key=${entry.key},value=${entry.value}")
    }

}