package com.kt.step4

//TODO 主构造函数

/*
1. 主构造函数的参数名用为_XXX是规范【这点我表示怀疑，因为这样命名会提示不符合规范】
2. 主构造函数中的参数，都是临时参数，无法直接使用，需要内部定义字符接收才能使用
 */
class Test03(_name:String){
    var name=_name
        get() = "[$field]"
    fun show() = println(name)
}
fun main() {
    Test03("sun").show()
}