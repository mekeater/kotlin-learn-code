package com.kt.step4
//TODO 主构造函数里定义属性。解决还需要内部再接收一下才能使用构造函数中的参数问题

class Test04(var name:String){
    fun show() = println(name) //可以直接使用构造函数中的参数，无需再接收一下。常用！
}
fun main() {
    Test04("hao").show()
}