package com.kt.step4
//TODO 定义类与field关键字学习

class Test
{
    /**
    实际上会先私有化这个字段，然后自动生成get set方法
    @NotNull
    private String name = "sun";

    @NotNull
    public final String getName() {
    return this.name;
    }

    public final void setName(@NotNull String var1) {
    Intrinsics.checkNotNullParameter(var1, "<set-?>");
    this.name = var1;
    }
     */
    var name = "sun"
    //我们也可以自己重写get set
    var address = "A"
        get() = "【$field】"
        set(value){
            field = "$value**"
        }
}

fun main() {
    println(Test().name)
    var test = Test()
    test.address = "ABC"
    println(test.address)
}