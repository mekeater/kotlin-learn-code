package com.kt.step4
//TODO构造函数中默认参数的学习
class Test06(_name:String = "sun"){
    //次构造比较调用主构造
    constructor(name:String="sun1", age:Int=18):this(name){
        println("次构造name=$name, age=$age")
    }
}
fun main() {
    Test06() //构造函数都有默认参数，这种情况，优先调用主构造函数
}