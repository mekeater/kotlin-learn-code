package com.kt.step4
//TODO 惰性初始化 by lazy

class Test10{

    val result by lazy { show() }  //用的时候再加载，尚未用到的时候，不加载
    private fun show(){
        println("开始显示")
        println("显示中...")
        println("显示中...")
        println("显示中...")
        println("显示中...")
        println("完成显示")

    }
}
fun main() {
    var test10 = Test10()
    println(test10.result) //如果不用懒加载，那么一开始运行，就加载了show，加了懒加载，只有执行到这一句才会加载
}