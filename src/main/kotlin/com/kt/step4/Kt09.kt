package com.kt.step4
//TODO 延迟初始化 lateinit
class Test09{
    lateinit var name:String //用的时候再初始化，不用就不初始化
    fun init(n:String){
        name = n
    }

    fun show(){
        if (::name.isInitialized){
            println("你的名字是$name")
        }else{
            println("还没有初始化")
        }
    }
}
fun main() {
    var test09 = Test09()
    test09.init("mekeater")
    test09.show()
}