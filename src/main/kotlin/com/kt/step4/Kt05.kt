package com.kt.step4
//TODO 次构造函数
class Test05(_name:String){
    //次构造比较调用主构造
    constructor(name:String, age:Int):this(name){
        println("次构造name=$name, age=$age")
    }
}
fun main() {
    var test05 = Test05("sun")
    var test051 = Test05("hao", 18)
}