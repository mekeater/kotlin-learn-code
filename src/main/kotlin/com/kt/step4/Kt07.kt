package com.kt.step4

//TODO 初始化代码块
class Test07(name:String){
    init {
        println("主构造函数调用了，name=$name") //主构造函数初始化。note:构造函数初始化中可以调用临时的传参
    }
    constructor(_name: String, _age:Int) : this(_name) {
        println("次构造函数调用了，name=$_name,age=$_age")
    }
    var userName = name
    fun show(){
        println(userName)
    }
}
fun main() {
    Test07("sun", 18).show()
}