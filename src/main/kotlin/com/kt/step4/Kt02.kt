package com.kt.step4

//TODO 计算属性与防范竞态条件
class Test02 {
    //计算属性，没有set,只有get
    val num: Int
        get() = (1..1000).shuffled().first() //返回随机数

    var info: String? = null
    fun getShowInfo(): String {
        //当调用成员有可能为null，就要采用下面的写法，这种写法称作 防范静态条件。专业的KT代码会有大量的这种写法
        return info?.let {
            if (it.isBlank()){
                "原来你是空值"
            }else{
                "info=$it"
            }
        } ?: "原来你是null"
    }
}

fun main() {
    println(Test02().num)
    println(Test02().getShowInfo())
}