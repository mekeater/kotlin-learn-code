package com.kt.step7.rxjava
//TODO 手写Rxjava
fun main() {
    create {
        "mekeater"
    }.map {
        "[$this]"
    }.map {
        "==$this=="
    }.observer {
        println(this)
    }
}
class RxjavaCoreClass<T>(val item:T)

//lambda对输入进行处理
inline fun<I> RxjavaCoreClass<I>.observer(observerAction:I.()->Unit):Unit = observerAction(item)

//对于输入进行处理，转换为输出给到RxjavaCoreClass
inline fun<I,O> RxjavaCoreClass<I>.map(mapAction:I.()->O) = RxjavaCoreClass(mapAction(item))
//create操作符只是将用户最后一行的输入原样输出给RxjavaCoreClass，起到一个输入参数的作用
inline fun<O> create(action:()->O) = RxjavaCoreClass(action())