package com.kt.base
//TODO when表达式，类似Java的switch语句
fun main() {
    var week = 6
    var info = when(week){
        1 -> "星期一"
        2 -> "星期二"
        3 -> "星期三"
        4 -> "星期四"
        5 -> "星期五"
        else -> {
            println("星期不知道") //Unit就是java中的void返回类型
        }
    }
    println(info)
}