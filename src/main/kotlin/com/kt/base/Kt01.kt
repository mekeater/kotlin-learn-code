package com.kt.base

// TODO 声明变量及内置数据类型
fun main() {
    /**
     * 声明变量
     * 可读可写  变量名  变量类型     值
     * var      name:  String =  "sun"
     */

    var name:String ="sun"
    name="hao"
    println(name);

    /**
     * 内置数据类型
     * String
     * Char
     * Boolean
     * Int
     * Double
     * List 集合
     * Set 无重复集合
     * Map 键值对
     * Int
     * Float
     */
}