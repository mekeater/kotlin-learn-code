package com.kt.base
//TODO 函数头学习
fun main() {
    method1(18, "sun")
}

//这样写比较合理简洁，但编译器最终还是会转换为我们常见的java代码
//不写修饰符，默认为public
private fun method1(age:Int, name:String):Int{
    println("你的名字是$name,你的年龄是$age")
    return 200
}