package com.kt.base
//TODO Nothing类型的特点
fun main() {
    method1(-1)
}

/**
 * public inline fun TODO(reason: String): Nothing = throw NotImplementedError("An operation is not implemented: $reason")
 */
private fun method1(score:Int){
    when(score){
        -1 -> TODO("没有这种情况")
        in 0..60 -> "不及格"
        in 61..70 -> "及格"
        in 71..100 -> "优秀"
    }
}