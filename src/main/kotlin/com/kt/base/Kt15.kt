package com.kt.base
//TODO 反引号（`）中函数名特点：反引号就是违反规则的方法
fun main() {
    `我的函数`("sun", 18)
}

private fun `我的函数`(name:String, age:Int){
    println("name:${name},age:$age")
}