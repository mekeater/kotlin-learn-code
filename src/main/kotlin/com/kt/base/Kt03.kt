package com.kt.base

// TODO 类型推断(可以推断出的变量类型，不需要写明)
fun main() {
    //val age:Int = 18
    val age = 18
    println(age)
}