package com.kt.base
//TODO String模板
fun main() {
    var address = "北京"
    var feel = "很好"
    println("今天我去${address}玩了，我感觉${feel}")

    //kotlin的if是表达式，更灵活，java的if是语句
    var isLogin = true
    println("当前登录状态 ${if (isLogin) "登录成功" else "登录失败"}")
}