package com.kt.step6;

public class Kt02java {
    public static void main(String[] args) {
        Person person = new Person();
        for (String name : person.getNames()) {
            System.out.println(name);
        }

        for (Integer age : person.age) {
            System.out.println(age);
        }
    }
}
