package com.kt.step6
//TODO @JvmField注解。可以通过反编译为java代码，查看加上JvmField注解与不加的区别，一看就懂了
class Person{
    val names = listOf("sun", "mekeater") //此时kotlin转为java代码会有一个get函数来拿这个值，因此在java中只能通过get函数拿这个属性
    @JvmField
    val age = listOf(18,19) //添加JvmField注解，此时kotlin转为java代码，不需要额外生成一个get函数，而是直接公开这个字段，java中直接当做属性直接调取，不需要再用get函数
}
fun main() {

}