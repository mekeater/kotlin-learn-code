package com.kt.step6
//TODO @JvmStatic注解学习:就是把kotlin的静态函数，共享给java使用。具体实现可以看反编译的java源码，一看就懂
class Kt04{
    companion object{
        @JvmField
        val name="mekeater"
        //不添加@JvmStatic注解，java无法直接调用该函数，需要看反编译java源码，来实现调用。Kt04.Companion.show();
        fun show() = println("name=$name")
        //添加@JvmStatic注解，java可以直接调用该函数，无需再加一个Companion类
        @JvmStatic
        fun show1() = println("name:$name")
    }
}
fun main() {
    Kt04.show()
}