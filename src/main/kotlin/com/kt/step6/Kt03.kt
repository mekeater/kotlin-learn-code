package com.kt.step6
//TODO @JvmOverloads注解学习:该注解将kotlin中的函数，带的默认参数值给java使用。

//此时在java中调用该函数，无法使用age的默认值，必须填写全部参数
fun show(name:String,age:Int = 18){
    println("name=$name,age=$age")
}

//通过JvmOverloads注解，就可以实现java中使用age的默认值
@JvmOverloads
fun show1(name:String,age:Int = 18){
    println("name=$name,age=$age")
}
fun main() {
    show("sun")
    show1("mekeater")
}