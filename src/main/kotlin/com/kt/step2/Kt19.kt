package com.kt.step2

import java.lang.Exception

//TODO 异常处理与自定义异常特点
fun main() {
    try {
        var info:String? = null

        checkException(info)

        println(info!!.length)

    }
    catch (e:Exception){
        println("哎呀：$e")
    }
}

fun checkException(info: String?) {
    info?:throw CustomException()
}
class CustomException:IllegalArgumentException("你写代码有点开玩笑！")
