package com.kt.step2
//TODO takeIf内置函数
fun main() {
    val name = "sun"
    val age = 18
    //如果takeIf返回的是ture,则返回调用者的值，否则返回null
    println(name.takeIf { age == 19 } ?: "${name}的年龄不是18")
}