package com.kt.step2
//TODO 匿名函数的类型推断
fun main() {
    //匿名函数写法一（无法进行类型推断）：方法名 : (入参类型) -> 返回类型 = {入参名 -> code body}
    //匿名函数写法二（可以进行类型推断，不需要写返回类型） 方法名 = {入参 -> code body}
    val method = {v1:Int, v2:String ->
            "v1:$v1,v2:$v2"
    }
    println(method(18, "sun"))
    //上面的写法同下面的写法
    val method1 : (Int, String) -> String = {v1, v2 ->
        "v1:$v1,v2:$v2"
    }
    println(method1(18, "hao"))
}