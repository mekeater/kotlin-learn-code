package com.kt.step2
//TODO takeUnless内置函数，该函数功能与takeIf相反
fun main() {
    val name = "sun"
    val age = 18
    //如果takeUnless返回的是false,则返回调用者的值，否则返回null
    println(name.takeUnless { age == 18 } ?: "${name}的年龄是18")
}