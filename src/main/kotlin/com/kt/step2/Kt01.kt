package com.kt.step2
//TODO 匿名函数的学习
fun main() {
    var count = "Mekeater".count()
    println(count)

    //匿名函数，如果看不懂，可以反编译为二进制的java代码理解
    var len = "Mekeater".count {
        it == 'e'
    }
    println(len)
}