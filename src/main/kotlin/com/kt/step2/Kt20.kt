package com.kt.step2

//TODO 先决条件函数
fun main() {
    var info: String? = null
    // java.lang.IllegalStateException: Required value was null.
    // checkNotNull(info)
    //java.lang.IllegalArgumentException: Required value was null.
    // requireNotNull(info)
    var select:Boolean = false
    //java.lang.IllegalArgumentException: Failed requirement.
    require(select)
}