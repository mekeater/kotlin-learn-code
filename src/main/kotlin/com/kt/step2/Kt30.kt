package com.kt.step2
//TODO run内置函数
fun main() {
    val str = "mekeater"
    str.run {
        // this 是str本身
        true //最后一行是返回值
    }

    //具名函数调用run
    str
            .run(::isLong)
            .run(::showText)
            .run(::mapText)
            .run(::println)
    //匿名函数调用run
    str
            .run {
                if (length>5) true else false
            }
            .run {
                if (this) "pass了" else "no pass 呀"
            }
            .run {
                "【$this】"
            }
            .run {
                println(this)
            }

}

fun isLong(str:String) = if (str.length>5) true else false
fun showText(isLong:Boolean) = if (isLong) "pass了" else "no pass 呀"
fun mapText(show:String) = "【$show】"