package com.kt.step2
//TODO 函数参数的学习
fun main() {
    //1. 函数声明 方法名:输入->输出，及函数实现【这种匿名写法与下面的函数效果一致】
    val method1:(String,Int)->String = {name, age ->
        "name:$name,age:$age"
    }
    //3. 函数调用
    println(method1("sun", 18))
}

private fun method1(name:String,age:Int):String{
    return "name:$name,age:$age"
}