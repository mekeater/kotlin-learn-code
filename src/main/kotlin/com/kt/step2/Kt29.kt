package com.kt.step2
//TODO let内置函数
fun main() {
    val name:String = "sun"
    //其实没什么说的，let中的it就是调用它的对象，可以在let中对调用对象进行处理，最后一行作为返回值。
    //后面研究它的实现方式，应该很有意思，现在只是觉得很累赘，把代码搞的可读性很低
    name.let {
        println("let中的值就是调用对象 $it")
    }
}