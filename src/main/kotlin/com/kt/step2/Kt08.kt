package com.kt.step2
//TODO 函数中定义参数为函数
fun main() {
    loginAPI("mekeater", "123") { msg:String, code:Int ->
        println("登录结果 msg:$msg,code:$code")
    }
}

//将函数通过lambda表达入参，如果用java，就需要用接口实现
fun loginAPI(name:String, pwd:String, responseResult: (String, Int) -> Unit)
{
    if (name == "mekeater" && pwd == "123")
    {
        responseResult("登录成功", 200)
    }
    else{
        responseResult("登录失败", 444)
    }
}