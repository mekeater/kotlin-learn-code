package com.kt.step2
//TODO 函数引用学习
fun main() {
    //lambda属于函数类型的对象，需要把responseMethod方法转变为 函数类型的对象（函数引用::）
    val obj = ::responseMethod
    login("mekeater", "123", obj)
}

private fun responseMethod(msg:String, code:Int){
    println("登录情况 msg:$msg, code:$code")
}

inline fun login(name:String,psw:String, responseResult:(String, Int)->Unit){
    if (name == "mekeater" && psw == "123")
    {
        responseResult("登录成功", 200)
    }
    else{
        responseResult("登录失败", 444)
    }
}