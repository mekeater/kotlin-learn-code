package com.kt.step2

import kotlin.math.roundToInt

//TODO Double转Int与类型格式化
fun main() {
    println(65.4321.roundToInt()) //四舍五入
    println(65.6789.roundToInt())
    println("%.3f".format(65.66666)) //四舍五入，并保留3位小数
}