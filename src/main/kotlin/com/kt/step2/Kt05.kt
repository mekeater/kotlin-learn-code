package com.kt.step2
//TODO it关键字特点，匿名函数，一个参数，默认参数名为it
fun main() {
    val method1:(Int)->String = {
        "it=$it"
    }
    println(method1(66))
}