package com.kt.step2

//TODO 函数类型作为返回类型
fun main() {
    var show = show("返回匿名函数")
    println(show("sun", 18))
}

fun show(info: String): (String, Int) -> String {
    println("info$info")
    return { name: String, age: Int ->
        "name:$name, age:$age"
    }
}