package com.kt.step2

//TODO 空合并操作符
fun main() {
    var info: String? = null
    info = "sun"
    //空合并操作符，如果 ?: 之前的值为null,则执行?:之后代码，否则执行值本身。
    println(info?: "info is null")
    //let函数和空合并操作符的联合使用
    println(info?.let { "[$it]" } ?: "[info is null]")
}