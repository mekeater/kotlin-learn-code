package com.kt.step2

//TODO 非空断言操作符特点
fun main() {
    var name: String? = null
    //使用!!是一种断言，效果和java就一样了，如果name为空指针，将会抛出异常，终止程序
    var capitalize = name!!.capitalize()
    println(capitalize)
}