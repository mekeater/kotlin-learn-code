package com.kt.step2
//TODO == 与 === 比较操作符
fun main() {
    val name1 = "Sun"
    val name2 = "sun".capitalize()
    println("name1=$name1 name2=$name2")
    // == 是内容的比较，java的equals
    println(name1 == name2)
    // === 引用的比较
    println(name1 === name2)
}