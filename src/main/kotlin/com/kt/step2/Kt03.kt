package com.kt.step2
//TODO 定义函数类型 & 隐式返回
fun main() {
    //1. 函数声明 方法名:输入->输出
    val method1:()->String
    //2. 函数实现
    method1 = {
        //匿名函数不用写return,最后一行就是返回值
        "success"
    }
    //3. 函数调用
    println(method1())
}