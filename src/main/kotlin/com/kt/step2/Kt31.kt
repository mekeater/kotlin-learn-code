package com.kt.step2
//TODO with内置函数
fun main() {
    val name = "sun"
    //将第一个参数传入后面的匿名函数/具名函数
    with(name, ::println)
}