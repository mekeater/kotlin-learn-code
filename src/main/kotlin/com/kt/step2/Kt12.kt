package com.kt.step2
//TODO 匿名函数与具名函数
fun main() {
    //匿名函数
    show("sun", "he is a success man") {
        println("result = $it")
    }

    //具名函数
    show("sun", "he is a success man", ::showResultImpl)
}

fun showResultImpl(result:String){
    println("result = $result")
}

inline fun show(name:String, info:String, showResult:(String)->Unit){
    val str = "name:$name,info:$info"
    showResult(str)
}