package com.kt.step2
//TODO apply内置函数
fun main() {
    val info = "Mekeater"
    //普通操作字符串
    println("len=${info.length}")
    println("last char=${info[info.length-1]}")
    println("all lowercase=${info.toLowerCase()}")
    println()
    //应用apply
    //一般匿名函数持有一个it，但apply不持有it，但持有this，this就是info本身，且apply始终返回的是info本身，因此可以通过链式调用处理info
    info.apply {
        println("len=${this.length}")
        println("len=$length") // this可以省略
    }.apply {
        println("last char=${this[length-1]}")
    }.apply {
        println("all lowercase=${toLowerCase()}")
    }
}