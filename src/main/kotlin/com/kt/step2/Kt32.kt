package com.kt.step2
//TODO also内置函数
fun main() {
    val name = "sun"
    //和apply类似，it就是name本身，返回的也是name本身，即内部处理，不影响返回的结果
    name.also {
        println(it)
    }.also {
        println(it.toUpperCase())
    }
}