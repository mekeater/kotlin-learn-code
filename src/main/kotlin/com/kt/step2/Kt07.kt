package com.kt.step2
//TODO lambda学习(匿名函数属于lambda)
fun main() {
    val add = {num1:Int, num2:Int ->
        "add = ${num1+num2}"
    }
    println(add(1,5))
    //匿名函数 入参 Int , 返回 Any类型
    //lambda表达式 参数 Int, lambda表达式结果是Any类型
    //匿名函数属于lambda表达式，即匿名函数也就是lambda表达式
    val week = {num:Int ->
        when(num){
            1 -> "星期1"
            2 -> "星期2"
            3 -> "星期3"
            4 -> "星期4"
            5 -> "星期5"
            else -> -1
        }
    }// week : (Int) -> Any
    println(week(1))
}