package com.kt.step2

//TODO 可空性特点
fun main() {
    var name: String = "sun"
    //Null can not be a value of a non-null type String
    //name = null

    var name1: String?
    //上面的声明，代表name可以为null，否则不允许为null
    name1 = null
    println(name1)
    name1 = "hao"
    println(name1)

}