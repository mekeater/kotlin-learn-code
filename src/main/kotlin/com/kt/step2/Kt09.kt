package com.kt.step2
//TODO 函数内联学习
/**
 * 函数中有lambda表达式，就应该声明为内联函数，否则调用端会生成多个对象完成lambda的调用，
 * 即将lambda转换为接口，然后后调用端实现接口对象。继而完成调用，会影响性能
 *
 * 是否为内联函数的区别，可以通过编译器转换的java代码看到区别。
 */

fun main() {
    loginAPI1("mekeater", "123") { msg:String, code:Int ->
        println("登录结果 msg:$msg,code:$code")
    }
}

//将函数通过lambda表达入参，如果用java，就需要用接口实现
inline fun loginAPI1(name:String, pwd:String, responseResult: (String, Int) -> Unit)
{
    if (name == "mekeater" && pwd == "123")
    {
        responseResult("登录成功", 200)
    }
    else{
        responseResult("登录失败", 444)
    }
}