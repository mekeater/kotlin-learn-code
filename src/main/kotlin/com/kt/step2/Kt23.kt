package com.kt.step2
//TODO replace 操作符
fun main() {
    val sourceStr = "ABCDEFGHIJKLMN"
    println("sourceStr=$sourceStr")
    var replace = sourceStr.replace(Regex("[AD]")) {
        when(it.value){
            "A" -> "9"
            "D" -> "6"
            else -> it.value
        }
    }
    println("replace=$replace")
}