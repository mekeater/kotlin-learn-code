package com.kt.step2
//TODO 数字类型的安全转换函数
fun main() {
//    val num1:Int = "666.6".toInt() //此时会异常
//    println(num1)
    val num2: Int? = "666.6".toIntOrNull() //此时转换失败会主动赋值为null，避免异常导致程序崩溃
    println(num2 ?: "转换异常啦")
}