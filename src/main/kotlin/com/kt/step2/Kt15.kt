package com.kt.step2

//TODO 使用带let的安全调用
fun main() {
    var name: String? = null
    name = ""
    //因为name是可空的，那么如果name为null，则？后的代码不执行，就不会引入空指针异常
    var let = name?.let {//确定name不为null的情况下，对name进行处理
        // it == name
        if (it.isBlank()) {
            "Default"
        } else {
            "$it"
        }
    }
    println(let)
}