package com.kt.step2

//TODO 安全调用操作符
fun main() {
    var name: String? = "sun"
    //因为name是可空的，那么如果name为null，则？后的代码不执行，就不会引入空指针异常
    var capitalize = name?.capitalize()
    println(capitalize)
}