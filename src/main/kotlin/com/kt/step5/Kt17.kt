package com.kt.step5
//TODO 接口定义
interface IUSB{
    //成员变量需要实现类通过构造函数，或者重写成员变量实现
    var usbVersion:String
    var usbInfo:String
    fun show():String
}

class Mouse(override var usbVersion: String, override var usbInfo: String) :IUSB{
    override fun show(): String {
        return "Mouse usbVersion=$usbVersion,usbInfo=$usbInfo"
    }
}

class Keyword():IUSB{
    override var usbVersion: String = ""
        get() = field
        set(value) {field=value}
    override var usbInfo: String = ""
        get() = field
        set(value) {field=value}

    override fun show(): String {
        return "Keyword usbVersion=$usbVersion,usbInfo=$usbInfo"
    }

}

fun main() {
    var usb:IUSB = Mouse("3.0", "usb3.0版本")
    println(usb.show())
    usb = Keyword()
    usb.usbVersion="3.1"
    usb.usbInfo="usb3.1"
    println(usb.show())
}