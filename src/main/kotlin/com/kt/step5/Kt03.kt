package com.kt.step5
//TODO 智能类型转换：如果上面一个类型用as转换过了，那么后续不需要用as,就能智能识别为是as转换后的类型，目前没理解存在的意义，只是为了少写点代码？
open class Person3(val name:String){
    //默认都是final方法，无法继承，需要用open关键字去除final
    open fun show() = println("人类name=$name")
    fun methodPerson() = println("这是person的方法")
}

class Student3(name: String):Person3(name){
    override fun show() {
        println("学生name=$name")
    }

    fun methodStudent() = println("这是student的方法")
}
fun main() {
    val p:Person3 = Student3("mekeater")
    (p as Student3).methodStudent()
    p.methodStudent()
}