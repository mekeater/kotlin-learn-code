package com.kt.step5
//TODO data类 解构声明学习

//普通类，声明解构函数，使其具备解构能力，但一般没必要，需要解构，就定义数据类就行了
class Test11(var name:String, var age:Int){
    //函数名必须为component1 component2 ...
    operator fun component1() = name
    operator fun component2() = age
}

data class Test11_1(var name:String, var age:Int)
fun main() {
    var test11 = Test11("mekeater", 18)
    val (name,age) = test11
    println("name=$name,age=$age")
    // 下划线代表不接收某个解构数据，可以看反编译java源码，理解实现方式
    val (name1, _) = Test11_1("sun", 18)
    println("name=$name1")
}