package com.kt.step5
//TODO 变换函数-map源码解析，完全能够代替rxjava的map操作符
fun main() {
    var list = listOf("A", "B", "C")
    /**
     * public inline fun <T, R> Iterable<T>.map(transform: (T) -> R): List<R> {
     *     return mapTo(ArrayList<R>(collectionSizeOrDefault(10)), transform)
     * }
     *
     * public inline fun <T, R, C : MutableCollection<in R>> Iterable<T>.mapTo(destination: C, transform: (T) -> R): C {
     *     for (item in this)
     *         destination.add(transform(item))
     *     return destination
     * }
     *
     * 把lambda表达式的返回结果（最后一行），添加到新的集合destination中，并返回。
     */
    list.map {
        "[$it]"
    }.map {
        "[$it, len=${it.length}]"
    }.map {
        print("$it ")
    }

}