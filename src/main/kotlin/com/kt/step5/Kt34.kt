package com.kt.step5
//TODO infix关键字学习：实现中缀表达式，不需要用.这样的符号调用扩展函数，简化代码。

//自定义中缀表达式
private infix fun <C1,C2> C1.go(c2:C2) {
    println("参数1：$this,参数二：$c2")
}
fun main() {
    /**
     * kotlin已经实现用于创建map对象的中缀表达式示例
     * /**
     *  * Creates a tuple of type [Pair] from this and [that].
     *  *
     *  * This can be useful for creating [Map] literals with less noise, for example:
     *  * @sample samples.collections.Maps.Instantiation.mapFromPairs
     *  */
     * public infix fun <A, B> A.to(that: B): Pair<A, B> = Pair(this, that)
     */
    mapOf("s" to 0)
    mapOf("u" to 1)

    //不使用infix关键字，只能通过下面的方式调用扩展函数
    "sun".go("mekeater")
    //使用infix关键字，也可以用过下面的方式调用扩展函数
    "you" go "good"
}