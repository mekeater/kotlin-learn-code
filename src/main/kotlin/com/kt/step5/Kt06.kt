package com.kt.step5
//TODO object : 对象表达式
open class Test06{
    open fun show(info:String) = println("[$info]")
}

//具名实现
class Test06Impl():Test06(){
    override fun show(info: String) {
        println("具名对象实现【$info】")
    }
}
fun main() {
    //匿名对象 表达式，即匿名实现类对象
    var test06 = object : Test06() {
        override fun show(info: String) {
            println("匿名对象实现【$info】")
        }
    }
    test06.show("mekeater")

    //具名对象
    var test06Impl = Test06Impl()
    test06Impl.show("sun")

    //对接口，用对象表达式（匿名对象）实现
    var run = object : Runnable {
        override fun run() {
            println("通过对象表达式，匿名实现接口")
        }
    }
    run.run()
}