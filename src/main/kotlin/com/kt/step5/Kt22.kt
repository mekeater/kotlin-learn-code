package com.kt.step5
//TODO 泛型类型约束
open class Animal()
open class Dog():Animal()
class BlackDog():Dog()
class Cat():Animal()
class Tree()
//T:Dog，T必须是Dog或者继承自Dog，相当于java中T extends Dog
class Test22<T:Dog>(private val input:T, private val isT:Boolean = true){
    fun getInput() = input.takeIf { isT }
}
fun main() {
    //println(Test22(Animal()).getInput())
    println(Test22(Dog()).getInput())
    println(Test22(BlackDog()).getInput())
    //println(Test22(Cat()).getInput())
    //println(Test22(Tree()).getInput())

}