package com.kt.step5
//TODO vararg关键字（动态参数）
class Kt23<T>(vararg var ages: T){
    fun show(index:Int) = println("age=${ages[index]}")

    //运算符重载
    operator fun get(index:Int) = ages[index]
}
fun main() {
    val p1 = Kt23(16,17.7,"sun")
    println(p1.show(2))
    println(p1[0])
}