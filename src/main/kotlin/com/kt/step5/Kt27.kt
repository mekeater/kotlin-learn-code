package com.kt.step5
//TODO reified关键字学习

/**
 * reified修饰的泛型，具有泛型类型判断的能力，即 obj is T
 */

data class ObjClass1(val name:String)
data class ObjClass2(val name:String)
class Kt27{
    //只有用reified修饰泛型，才有泛型类型判断的能力 it is T。同时只有inline修饰的函数，才能使用reified修饰
    inline fun<reified T> getObj(defaultLambdaAction:()->T):T{
        var listOf = listOf(ObjClass1("obj1"), ObjClass2("obj2"))
        var obj = listOf.shuffled().first()
        println("随机对象：$obj")
        //T? 代表可为null,不然会有null as T转换异常
        return obj.takeIf { it is T} as T? ?: defaultLambdaAction()
    }
}
fun main() {
    Kt27().getObj<ObjClass1> {
        var result = ObjClass1("备用对象")
        println("启用备用对象:$result")
        result
    }
}