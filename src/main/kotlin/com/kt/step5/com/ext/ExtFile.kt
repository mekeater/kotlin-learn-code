package com.kt.step5.com.ext

//定义扩展函数，获取随机值
fun<E> Iterable<E>.random():E{
    return this.shuffled().first()
}

//定义扩展函数，获取随机值，并输出
fun<I> Iterable<I>.randomPrint(){
    println(this.shuffled().first())
}