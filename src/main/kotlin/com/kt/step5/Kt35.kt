package com.kt.step5

import com.kt.step5.com.ext.random
import com.kt.step5.com.ext.randomPrint

//TODO 定义扩展文件（其实就是在一个指定的文件中，写大量的扩展函数，然后在其它地方通过导包使用，这样比较规范）
fun main() {
    var list = listOf("sun", "mekeater", 18)
    var set = setOf("hello", true, 18)
    println(list.shuffled().first())
    println(set.shuffled().first())

    println()

    //使用扩展文件中的扩展函数
    println(list.random())
    set.randomPrint()
}