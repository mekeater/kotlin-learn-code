package com.kt.step5
//TODO 使用协变 out 和逆变 in
/**
 * 协变 out T: 1. 泛型只能读取，不能修改 2. 子类泛型可以直接赋值给父类泛型，类似java ? extend T
 * 逆变 in T：1. 泛型只能修改，不能读取 2. 父类泛型可以直接赋值给子类泛型，类似java ? super T
 */
fun main() {

}