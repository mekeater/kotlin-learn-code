package com.kt.step5
//TODO 超类上定义扩展函数
data class TestClass1(val name:String)
fun Any.show(): Any {
    println(this)
    return this  //可以实现链式调用
}
fun main() {
    TestClass1("mekeater").show().show()
}