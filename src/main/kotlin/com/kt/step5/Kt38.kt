package com.kt.step5
//TODO DSL编程范式(Domain Specified Language) 领域专用语言，定义输入输出等规则

//入门DSL编程范式的示例
class Context{
    val info = "Context DSL"
    val name = "sun"
    fun toast(str:String) = println("弹出${str}提示")
}

/**
 * 输入规则：必须Context类才能使用applyDSL，  同时持有this 和 it
 * 输出规则：始终返回Context本身
 */
inline fun Context.applyDSL(lambda:Context.(str:String)->Unit):Context{
    lambda(info)
    return this
}

fun main() {
    Context().applyDSL {
        toast(it)
        toast(this.name)
        toast("mekeater")
    }.applyDSL {  }
}