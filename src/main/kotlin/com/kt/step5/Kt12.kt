package com.kt.step5
//TODO 运算符重载
data class Add(var num1:Int, var num2:Int){
    //实现 + 运算符重载
    operator fun plus(p1:Add): Int {
        return (num1+p1.num1) + (num2+p1.num2)
    }

    //下面的方式，会提示所有，可以重载的运算符的名字
    //operator fun Add.
}
fun main() {
    println(Add(1,2) + Add(3,4))
}