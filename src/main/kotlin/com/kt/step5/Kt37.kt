package com.kt.step5
//TODO apply函数源码分析

/**
 * inline 为了优化lambda表达式的性能
 * fun <INPUT> 定义泛型函数
 * INPUT.mApply 添加泛型扩展函数
 * lambda:INPUT.()->Unit lambda表达，INPUT.()是对泛型进行扩展，使得lambda表达式持有this
 * 具体实现，调用lambda，并最终返回INPUT本身
 * {
 *     lambda()
 *     return this
 * }
 */
inline fun <INPUT> INPUT.mApply(lambda:INPUT.()->Unit):INPUT{
    lambda()
    return this
}

fun main() {
    "sun".apply {
        println(this)
    }.apply {
        println(this)
    }

    println()

    "mekeater".apply {
        println(this)
    }.apply {
        println(this)
    }
}