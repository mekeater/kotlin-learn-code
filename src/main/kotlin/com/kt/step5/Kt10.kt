package com.kt.step5
//TODO data类 cope函数 toString hashCode equals 解构操作等只是处理主构造的参数，所以，次构造的内容会丢失，看反编译java代码可以详细了解
data class Test10(var name:String, var age:Int){
    var info:String = ""
    constructor(name: String):this(name, 18){
        info = "this is a importance info"
    }

    override fun toString(): String { //data类 toString只会考虑主构造，因此不会有info信息，在这重写一下
        return "name:$name,age:$age,info:$info"
    }
}
fun main() {
    var test10 = Test10("mekeater")
    println(test10)
    var copy = test10.copy(name = "sun") //data类 cope函数也只会考虑主构造，因此，次构造中赋值的info信息会丢失
    println(copy)
}