package com.kt.step5
//TODO 函数式编程理解
fun main() {
    val name = listOf("sun", "mekeater")
    val age = listOf(18,19)
    //就下面这一句函数式编程，用普通的java代码写，需要写一堆才能实现
    name.zip(age).toMap().map { "name:${it.key},age:${it.value}"}.map { println(it) }
}