package com.kt.step5
//TODO 嵌套类学习
class Test08{
    val info = "mekeater"
    //不加inner默认是嵌套类，必须加inner使得嵌套的类成为内部类，而内部类才能访问外部类的成员变量
    inner class Test1{
        fun show() = println("info=$info")
    }
}
fun main() {
    Test08().Test1().show()
}