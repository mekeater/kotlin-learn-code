package com.kt.step5
//TODO 对象声明：实现单例实例

//object 就是实现一个单例对象。可以反编译查看java代码的实现。
object Test5{
    //因为是单例，主构造函数用不到了，所以，init实际写在静态代码块了，可以反编译查看
    init {
        println("Test5 init")
    }

    fun show() = println("show函数")
}
fun main() {
    //都是同一个实例
    println(Test5)
    println(Test5)
}