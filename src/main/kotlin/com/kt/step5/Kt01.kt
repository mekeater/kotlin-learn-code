package com.kt.step5

//TODO 继承与重载的open关键字
//默认都是final类无法继承，需要用open关键字去除final
open class Person(val name:String){
    //默认都是final方法，无法继承，需要用open关键字去除final
    open fun show() = println("人类name=$name")
}

class Student(name: String):Person(name){
    override fun show() {
        println("学生name=$name")
    }
}
fun main() {
    var person:Person = Student("MEKEATER")
    person.show()
}