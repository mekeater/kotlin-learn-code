package com.kt.step5;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class Kt43_java {
    public static void main(String[] args) {
        List<String> names=new ArrayList<>();
        names.add("sun");
        names.add("mekeater");

        List<Integer> ages =new ArrayList<>();
        ages.add(18);
        ages.add(19);

        Map<String, Integer> map =new LinkedHashMap<>();
        for (int i = 0; i < names.size(); i++) {
            map.put(names.get(i), ages.get(i));
        }

        List<String> newList=new ArrayList<>();
        for (Map.Entry<String, Integer> stringIntegerEntry : map.entrySet()) {
            newList.add("name:"+stringIntegerEntry.getKey()+" age:"+stringIntegerEntry.getValue());
        }

        for (String s : newList) {
            System.out.println(s);
        }
    }

}
