package com.kt.step5
//TODO 代数数据类型（其实就是把枚举类和when结合使用的场景）
enum class Score{
    A,
    B,
    C,
    D
}
class Teacher(private val score:Score){
    fun show() = when(score){
        Score.A -> "成绩为A"
        Score.B -> "成绩为B"
        Score.C -> "成绩为C"
        Score.D -> "成绩为D"
    }
}
fun main() {
    println(Teacher(Score.A).show())
}