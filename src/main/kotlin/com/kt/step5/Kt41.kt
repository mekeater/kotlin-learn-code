package com.kt.step5
//TODO 过滤函数-filter源码解析
fun main() {
    var list = listOf("A", "B", "C")
    list.map {
        /**
         * public inline fun String.filter(predicate: (Char) -> Boolean): String {
         *     return filterTo(StringBuilder(), predicate).toString()
         * }
         *
         * public inline fun <C : Appendable> CharSequence.filterTo(destination: C, predicate: (Char) -> Boolean): C {
         *     for (index in 0 until length) {
         *         val element = get(index)
         *         if (predicate(element)) destination.append(element)
         *     }
         *     return destination
         * }
         *
         * 如果lambda表达式的返回值为true，则遍历当前元素，加入新的字符串构造器StringBuilder destination，否则，不加入新的字符串。最后返回新的字符串
         */
        it.filter { it ->
            println("$it filter")
            it == 'A'
        }
    }.map {
        print("$it  ")
    }
}