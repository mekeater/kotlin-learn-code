package com.kt.step5
//TODO 定义泛型类
class Kt19<T>(private val obj:T){
    fun show() = println("obj=$obj")
}
fun main() {
    Kt19<Int>(18).show()
    Kt19("hello").show()
}