package com.kt.step5
//TODO 泛型扩展函数

fun <T> T.showContent(){
    if (this is String)
        println("字符串长度为：$length")
    else
        println("不是字符串")
}

fun <I> I.showTime() = println("当前时间:${System.currentTimeMillis()}")

fun main() {
    //所有类型，都是泛型
    234.showContent()
    "mekeater".showContent()
    567.showTime()
}