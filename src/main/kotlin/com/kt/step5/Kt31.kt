package com.kt.step5
//TODO 标准函数与泛型扩展函数（标准函数源码分析/模仿）

/**
 * private 私有化
 * inline 因为有lambda表达式，用于优化性能
 * fun <I,O> 声明泛型，I代表输入类型，O代表输出类型
 * I.mLet 对于I输入类型增加函数扩展
 * block:(I) -> O mLet函数中需要传入的lambda表达式
 * block(this)  mLet函数内部实现，调用lambda表达式，传入的参数this代表I类型的对象本身
 */
private inline fun <I,O> I.mLet(block:(I) -> O) : O= block(this)
fun main() {
    var mLet = 345.mLet {
        "sun"
    }
    println(mLet)

    345.let {
        true
    }
}