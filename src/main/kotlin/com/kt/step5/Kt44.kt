package com.kt.step5
//TODO kotlin与java进行交互，及可空性
fun main() {
    println(Kt44_java().info1.length)
    //println(Kt44_java().info2.length) //会引发空指针crash

    //采用下面的写法才是规范的，可以避免空指针异常
    var info2 : String? = Kt44_java().info2
    println(info2?.length)
}