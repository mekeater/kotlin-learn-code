package com.kt.step5
//TODO 泛型变换
class Test21<T>(val isMap:Boolean,val inputData:T){
    //模仿rxjava实现类型转换
    inline fun<R> map(mapAction:(T)->R) = mapAction(inputData).takeIf { isMap }
}

inline fun <I, O> map(input: I, action: (I) -> O) =
        action(input)

fun main() {
    println(Test21(true, 18).map { "青春年少:${it}岁" } ?: "你是null呀")
    var map = map(123) {
        "[$it]"
    }
    println(map)
}