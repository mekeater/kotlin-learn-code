package com.kt.step5
//TODO in-逆变
// 子类 = 父类
class Consumer0:Consumer<Animal1>{
    override fun consumer(item: Animal1) {
        println("消费 Animal1")
    }
}

class Consumer1:Consumer<Dog1>{
    override fun consumer(item: Dog1) {
        println("消费 Dog1")
    }
}
fun main() {
    //父类泛型直接赋值给了子类泛型，原因是in起的作用，类似java中? super Dog1
    val p1:Consumer<Dog1> = Consumer0()
    val p2:Consumer<Dog1> = Consumer1()
}