package com.kt.step5

//TODO 枚举类定义函数（带参数的枚举类），一般枚举类的参数都要私有化，因此参数值是在内部枚举值中传入的，不需要外部传入
enum class Info(private var status:String){
    START("开始"),
    STOP("停止"); //枚举后面如果有函数，需要有结束符
    fun show() = println("status = $status")
    fun update(s:String){
        this.status = s
    }
}
fun main() {
    Info.START.show()
    Info.STOP.show()

    Info.START.update("启动")
    Info.START.show()
}