package com.kt.step5

import com.kt.step4.Test09

//TODO 数据类学习，对应javaBean的场景

//可以通过反编译为java代码比较普通类和数据类的区别。
//普通类只有get set和构造函数，而数据类相对于普通类，会再自动生成toString hashCode equals 解构操作
class Test09(var name:String){

}

data class Test091(var name: String){

}
fun main() {
    println(Test09("mekeater"))
    println(Test091("mekeater")) //因为数据类自动重写了toString,所以，输出结果与普通类不一样
}