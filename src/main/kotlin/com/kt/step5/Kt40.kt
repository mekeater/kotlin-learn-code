package com.kt.step5
//TODO 变换函数-flatmap 源码解析
fun main() {
    var list = listOf("A", "B", "C")
    /**
     * public inline fun <T, R> Iterable<T>.flatMap(transform: (T) -> Iterable<R>): List<R> {
     *     return flatMapTo(ArrayList<R>(), transform)
     * }
     *
     * public inline fun <T, R, C : MutableCollection<in R>> Iterable<T>.flatMapTo(destination: C, transform: (T) -> Iterable<R>): C {
     *     for (element in this) {
     *         val list = transform(element)
     *         destination.addAll(list)
     *     }
     *     return destination
     * }
     *
     * lambda表达式的返回结果是一个集合，把lambda表达式的返回集合，通过addAll函数铺开，并添加到新的集合destination中，最后返回新的集合。
     */
    list.flatMap {
        listOf("[$it ,", "$it]")
    }.map {
        print("$it ")
    }
}