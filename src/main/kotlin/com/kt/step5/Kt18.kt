package com.kt.step5
//TODO 抽象类 和java一样，没啥好说的

abstract class BaseActivity{
    fun onCreate(){
        setContentView(getLayoutID())
        initView()
    }

    private fun setContentView(layoutID:Int) = println("layoutID=$layoutID")

    abstract fun getLayoutID():Int
    abstract fun initView()
}

class MainActivity: BaseActivity() {
    override fun getLayoutID(): Int = 666

    override fun initView() = println("initView finish")
}

fun main() {
    MainActivity().onCreate()
}