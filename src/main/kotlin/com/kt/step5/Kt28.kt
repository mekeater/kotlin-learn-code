package com.kt.step5
//TODO 定义扩展函数学习:可以给一个类添加新的自定义函数
class Kt28(val name:String)

// 增加扩展函数
fun Kt28.show() = println("Kt28类的扩展函数，name=$name")
//每个扩展函数都拥有一个类对象本身this
fun String.addExtend(num:Int) = this + "#".repeat(num)

fun main() {
    Kt28("sun").show()
    println("mekeater".addExtend(6))
}