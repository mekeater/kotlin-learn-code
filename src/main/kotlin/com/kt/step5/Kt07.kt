package com.kt.step5
//TODO 伴生对象学习，代替java的static
class Test07{
    companion object{
        val info = "mekeater"
        fun show() = println("info=$info")
    }
}
fun main() {
    Test07.show()
}