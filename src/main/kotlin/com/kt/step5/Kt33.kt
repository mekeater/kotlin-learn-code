package com.kt.step5
//TODO 可空类型扩展函数

//可空的扩展函数
fun String?.default(value:String) = this ?: value
fun main() {
    var str: String? = null
    println(str.default("你是null呀"))
    str = "sun"
    println(str.default("你是null呀"))
}