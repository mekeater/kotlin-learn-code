package com.kt.step5
//TODO 合并函数-zip源码解析
fun main() {
    val name = listOf("sun", "mekeater")
    val age = listOf(18,19)

    /**
     * public infix fun <T, R> Iterable<T>.zip(other: Iterable<R>): List<Pair<T, R>> {
     *     return zip(other) { t1, t2 -> t1 to t2 }
     * }
     *
     * public inline fun <T, R, V> Iterable<T>.zip(other: Iterable<R>, transform: (a: T, b: R) -> V): List<V> {
     *     val first = iterator()
     *     val second = other.iterator()
     *     val list = ArrayList<V>(minOf(collectionSizeOrDefault(10), other.collectionSizeOrDefault(10)))
     *     while (first.hasNext() && second.hasNext()) {
     *         list.add(transform(first.next(), second.next()))
     *     }
     *     return list
     * }
     *
     * 通过lambda表达式将第一个集合和另一个集合，以Pair的形式添加到新的集合中，并返回
     */
    var zip = name.zip(age)
    zip.toMap().forEach{
        println("name=${it.key},age=${it.value}")
    }
}