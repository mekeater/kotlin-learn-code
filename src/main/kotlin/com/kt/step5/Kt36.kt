package com.kt.step5
//通过as重命名扩展函数名
import com.kt.step5.com.ext.random as r
import com.kt.step5.com.ext.randomPrint as rp
//TODO 重命名扩展函数
fun main() {
    var list = listOf("sun", "mekeater", 18)
    var set = setOf("hello", true, 18)
    println(list.r())
    set.rp()
}