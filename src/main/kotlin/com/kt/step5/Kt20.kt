package com.kt.step5
//TODO 泛型函数

fun main() {
    show(10)
    show(10.66)
    show("good")
    show(null)
}

fun<T> show(info:T){
    info?.also {
        println("info=$it")
    } ?: println("info=null")
}