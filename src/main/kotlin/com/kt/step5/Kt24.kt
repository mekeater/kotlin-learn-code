package com.kt.step5
//TODO out-协变
// 父类 = 子类
/*
 out T定义的泛型，称之为协变：只能被读取，不能被修改。
 同时需要注意，协变的泛型，可以将子类对象赋值给父类对象，这时的out，就相当于java的? extend T修饰的泛型
 in T 定义的泛型，称之为逆变：只能被修改，不能被读取
 */

interface Product<out T>{
    //修改操作，无法通过编译
    //fun f1(item:T)
   fun product():T
}
interface Consumer<in T>{
    fun consumer(item:T)
    //读取操作，无法通过编译
    //fun f2():T
}

open class Animal1
open class Dog1: Animal1()
class Product0:Product<Animal1>{
    override fun product(): Animal1 {
        println("生产 Animal1")
        return Animal1()
    }
}

class Product1:Product<Dog1>{
    override fun product(): Dog1 {
        println("生产 Dog1")
        return Dog1()
    }
}

fun main() {
    val p1:Animal1 = Product0().product()
    // 子类Dog1可以赋值给父类Animal1,正常子类是不能赋值给父类的，
    // 此处之所以可以，是因为itf1中的泛型是采用协变，类似java的 ? extend Animal T
    val p2:Animal1 = Product1().product()
}