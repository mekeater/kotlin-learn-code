package com.kt.step5
//TODO 密封类学习，其实就是密封一堆类
sealed class Person1 {
    object Bird:Person1()
    class Student(var name:String):Person1()
}

class Test16(private var person1:Person1){
    fun show() = when(person1){
        is Person1.Student -> "这是一个学生，学生姓名：${(person1 as Person1.Student).name}"
        is Person1.Bird -> "这是一只小鸟"
    }
}
fun main() {
    println(Test16(Person1.Student("mekeater")).show())
    println(Test16(Person1.Bird).show())
}